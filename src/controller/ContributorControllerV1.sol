// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "@le7el/web3_crs/contracts/controller/GatedController.sol";

contract ContributorControllerV1 is GatedController {
    constructor (IBaseNFT _base) GatedController(_base) {}
}