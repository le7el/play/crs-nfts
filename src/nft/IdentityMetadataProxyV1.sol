// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "@le7el/generative_art/src/SVG9x9.sol";
import "@le7el/generative_art/src/EncodeUtils.sol";
import "@le7el/web3_crs/contracts/resolver/proxies/ITokenURIProxy.sol";

/** 
 * @dev Use pixel cloud as default image, unless set by user.
 */
contract IdentityMetadataProxyV1 is ITokenURIProxy {
    string public constant SUFFIX = ".l7l";
    bytes32 public constant EMPTY_STRING_KECCAK = 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470;

    /**
     * Returns proxy contract address which resolves into some content.
     * @param _name Domain name.
     * @param _customImage URL to custom image.
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256, string memory _name, string memory _customImage) external pure returns (string memory) {
        string memory _fullname = string(abi.encodePacked(_name, SUFFIX));
        string memory metadata = string(abi.encodePacked(
            "{",
                EncodeUtils.attributeNameAndValue("name", _fullname, false, false),
                EncodeUtils.attributeNameAndValue("description", "LE7EL identity record", false, false),
                (keccak256(abi.encodePacked(_customImage)) == EMPTY_STRING_KECCAK ?
                    EncodeUtils.attributeNameAndValue("image_data", SVG9x9.renderPixelCloud(_fullname), false, false) :
                    EncodeUtils.attributeNameAndValue("image", _customImage, false, false)),
                "\"attributes\":",
                _compileAttributes(),
            "}"
        ));

        return string(abi.encodePacked(
            "data:application/json;base64,",
            EncodeUtils.base64(bytes(metadata))
        ));
    }

    /**
    * @dev Sets the attributes of the NFTs
    *
    * @return string with generated attributes.
    */
    function _compileAttributes() internal pure returns (string memory) {
        string memory _type = "Identity";
        return string(abi.encodePacked(
            "[",
            EncodeUtils.attributeForTypeAndValue("Type", _type, false),
            "]"
        ));
    }
}