// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@le7el/web3_crs/contracts/nft/BaseNFT.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ITextResolver.sol";

/** 
 * @dev Metadata generation is delegated to updatable proxy contract.
 *      To disable trading of identity NFTs, approve functionality is paused,
 *      Approvals can be enabled by owner at some later point.
 */
contract IdentityNFTV1 is BaseNFT, Pausable {
    constructor(ICRS _crs, bytes32 _baseNode) ERC721("LE7EL Identity v1", "IDENTITY") {
        crs = _crs;
        baseNode = _baseNode;
        _pause();
    }

    /**
     * @dev Owner can allow trading through approvals.
     */
    function ownerToggleApprovals(bool _state) external onlyOwner {
        if (_state) {
            _unpause();
        } else {
            _pause();
        }
    }

    /**
     * @dev Identity NFTs are not for trading, disable default approval interfaces.
     */
    function approve(address to, uint256 tokenId) public override(ERC721, IERC721) whenNotPaused {
        super.approve(to, tokenId);
    }

    /**
     * @dev Identity NFTs are not for trading, disable default approval interfaces.
     */
    function setApprovalForAll(address operator, bool approved) public override(ERC721, IERC721) whenNotPaused {
        super.setApprovalForAll(operator, approved);
    }

    /**
     * @dev NFT metadata.
     * @param _id The token ID (keccak256 of the label).
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256 _id) public override(BaseNFT) view returns (string memory) {
        bytes32 _baseNode = baseNode;
        ICRS _crs = crs;
        address _proxy = IProxyConfigResolver(_crs.resolver(_baseNode)).proxyConfig(_baseNode, address(this), TOKEN_URI_SELECTOR);
        bytes32 _nodehash = keccak256(abi.encodePacked(_baseNode, bytes32(_id)));
        
        string memory _customAvatar = "";
        address _resolver = _crs.resolver(_nodehash);
        if (_resolver != address(0)) _customAvatar = ITextResolver(_resolver).text(_nodehash, "L7L_IDENTITY_IMAGE");
        return ITokenURIProxy(_proxy).tokenURI(_id, names[_id], _customAvatar);
    }
}
