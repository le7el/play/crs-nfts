// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "@le7el/web3_crs/contracts/nft/BaseNFT.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ITextResolver.sol";
import "../resolver/profile/ILevelResolver.sol";

/** 
 * @dev Metadata generation is delegated to updatable proxy contract.
 * Contributor NFTs can be transfered, but trading is not encouraged by
 * disabling approvals, so it's not possible to trade on public marketplaces.
 */
contract ContributorNFTV1 is BaseNFT {
    error TradingForbidden();
    
    constructor(ICRS _crs, bytes32 _baseNode) ERC721("LE7EL Contributor v1", "CONTRIBUTOR") {
        crs = _crs;
        baseNode = _baseNode;
    }

    /**
     * @dev Contributor NFTs are not for trading, disable default approval interfaces.
     */
    function approve(address, uint256) public override(ERC721, IERC721) {
        revert TradingForbidden();
    }

    /**
     * @dev Contributor NFTs are not for trading, disable default approval interfaces.
     */
    function setApprovalForAll(address, bool) public override(ERC721, IERC721) {
        revert TradingForbidden();
    }

    /**
     * @dev NFT metadata.
     * @param _id The token ID (keccak256 of the label).
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256 _id) public override(BaseNFT) view returns (string memory) {
        bytes32 _baseNode = baseNode;
        ICRS _crs = crs;
        address _proxy = IProxyConfigResolver(_crs.resolver(_baseNode)).proxyConfig(_baseNode, address(this), TOKEN_URI_SELECTOR);
        bytes32 _nodehash = keccak256(abi.encodePacked(_baseNode, bytes32(_id)));
        
        string memory _customContributor = "";
        address _resolver = _crs.resolver(_nodehash);
        if (_resolver != address(0)) _customContributor = ITextResolver(_resolver).text(_nodehash, "L7L_CONTRIBUTOR_IMAGE");
        return ITokenURIProxy(_proxy).tokenURI(_id, names[_id], _customContributor);
    }
}