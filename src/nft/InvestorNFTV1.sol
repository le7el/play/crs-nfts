// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import {ERC721} from "solmate/tokens/ERC721.sol";

/** 
 * @dev Investor NFT contract.
 */
contract InvestorNFTV1 is ERC721 {
    uint256 public constant MAX_INDEX = 2222;
    
    // @see ../minter/InvestorNFTMinterV1.sol for implementation.
    // solhint-disable-next-line var-name-mixedcase
    address public immutable MINTER;
    
    constructor(address _minter) ERC721("L7 Investors", "L7IN") {
        MINTER = _minter;
    }

    /**
     * @dev Mint new NFT.
     *
     * @param _owner Address which will own minted NFT.
     * @param _id Id of NFT to mint.
     */
    function mintTo(address _owner, uint256 _id) external {
        require(msg.sender == MINTER, "Not a minter.");
        require(_id > 0 && _id <= MAX_INDEX, "Sorry, sold out.");
        super._safeMint(_owner, _id);
    }

    /**
     * @dev NFT metadata.
     *
     * @param _id The token ID (keccak256 of the label).
     * @return Metadata URI.
     */
    function tokenURI(uint256 _id) public override(ERC721) view virtual returns (string memory) {
        return string(abi.encodePacked("https://l7in.le7el.com/v1/metadata/", _uint2str(_id)));
    }

    /**
     * @dev Convet uint256 into string for rendering.
     *
     * @param _i number to convert.
     * @return _uintAsString number in a string form
     */
    function _uint2str(uint _i) internal pure returns (string memory _uintAsString) {
        if (_i == 0) {
            return "0";
        }
        uint j = _i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len;
        while (_i != 0) {
            k = k-1;
            uint8 temp = (48 + uint8(_i - _i / 10 * 10));
            bytes1 b1 = bytes1(temp);
            bstr[k] = b1;
            _i /= 10;
        }
        return string(bstr);
    }
}