// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface IVirtualDistributor {
    function pendingRewards(address _nftContract, uint256 _tokenId) external view returns (uint256);
    function isRewardSafelyLocked(address _nftContract, uint256 _tokenId) external view returns (bool);
}