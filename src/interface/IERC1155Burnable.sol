// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface IERC1155Burnable {
    function burn(address account, uint256 id, uint256 value) external; // 0xf5298aca
}