// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface IMintable {
    function mintTo(address owner, uint256 id) external;
}