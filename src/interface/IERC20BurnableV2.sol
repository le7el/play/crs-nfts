// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface IERC20BurnableV2 {
    function burn(address account, uint256 value) external; // 0x9dc29fac
}