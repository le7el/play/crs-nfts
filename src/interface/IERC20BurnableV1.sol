// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface IERC20BurnableV1 {
    function burnFrom(address account, uint256 value) external; // 0x79cc6790
}