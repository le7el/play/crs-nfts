// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface ILevelingFormula {
    /**
     * @dev Get level based on experience.
     * @param _experience experience points.
     * @return user level based on experience.
     */
    function expToLevel(uint256 _experience) external view returns (uint256);
}