// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

interface ILevelResolver {
    event AdvancedToNextLevel(
        bytes32 indexed project,
        bytes32 indexed node,
        uint256 newExperience,
        uint256 totalExperience
    );

    event ProjectLevelingRulesChanged(
        bytes32 indexed project,
        bytes4 indexed burnInterface,
        address indexed experienceToken,
        uint256 experienceTokenId,
        address levelingFormulaProxy
    );

    /**
     * @dev Level based on experience.
     * @param _project node for a project which issue experience.
     * @param _node the node to query.
     * @return level based on experience
     */
    function level(bytes32 _project, bytes32 _node) external view returns (uint256);

    /**
     * @dev Experience in scope of project.
     * @param _project node for a project which issue experience.
     * @param _node the node to query.
     * @return project experience
     */
    function experience(bytes32 _project, bytes32 _node) external view returns (uint256);
}