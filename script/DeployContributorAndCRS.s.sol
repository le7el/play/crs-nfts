// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import "@le7el/web3_crs/contracts/registry/CRSRegistry.sol";
import "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {ContributorControllerV1} from "src/controller/ContributorControllerV1.sol";
import {ContributorNFTV1} from "src/nft/ContributorNFTV1.sol";
import {LevelingResolver} from "src/resolver/LevelingResolver.sol";

contract DeployContributorAndCRSScript is Script {
    bytes32 internal contributorBasenode = 0x9ccff02751c1b99e2c60d3b7d6db94c388e4e1828dd065231a867e115b8713e6; // ENS namehash
    bytes32 internal contributorHash = 0xa0921f37fe9761e5e4ec5aa1bd4de740658170e3cf81b3920e874799011ab58b; // Keccak256
    CRSRegistry internal crs;
    LevelingResolver internal resolver;
    ContributorNFTV1 internal contributorNFT;
    ContributorControllerV1 internal contributorController;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address crs_address = vm.envAddress("CRS_REGISTRY_ADDRESS");
        if (crs_address != address(0)) {
            crs = CRSRegistry(crs_address);
            crs.setSubnodeOwner(bytes32(0), contributorHash, msg.sender);
        } else {
            crs = new CRSRegistry();
            console.log("CRS registry: ");
            console.log(address(crs));
            crs.setSubnodeOwner(bytes32(0), contributorHash, msg.sender);
        }
        address resolver_address = vm.envAddress("CRS_RESOLVER_ADDRESS");
        if (resolver_address != address(0)) {
            resolver = LevelingResolver(resolver_address);
        } else {
            resolver = new LevelingResolver(crs);
            console.log("Resolver: ");
            console.log(address(resolver));
        }

        contributorNFT = new ContributorNFTV1(crs, contributorBasenode);
        console.log("Contributor NFT: ");
        console.log(address(contributorNFT));
        contributorController = new ContributorControllerV1(contributorNFT);
        console.log("Contributor NFT controller: ");
        console.log(address(contributorController));
        contributorNFT.addController(address(contributorController));

        // Will fail if you try to override one of existing zones
        crs.setResolver(contributorBasenode, address(resolver));
        resolver.setRoyalties(contributorBasenode, address(contributorController), 0, address(0), address(0)); // free
        crs.setSubnodeOwner(bytes32(0), contributorHash, address(contributorNFT));

        // Set leveling rules
        address exp_token = vm.envAddress("EXP_TOKEN_ADDRESS");
        if (exp_token != address(0)) {
            contributorNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setProjectLevelingRules(bytes32,address,address,uint256,bytes4)",
                contributorBasenode,
                address(0),
                exp_token,
                0,
                bytes4(0xf5298aca)
            ));
        }

        // Set level metadata
        address virtual_distributor = vm.envAddress("VIRTUAL_DIST_ADDRESS");
        if (virtual_distributor != address(0)) {
            contributorNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setText(bytes32,string,string)",
                contributorBasenode,
                "L7L_REWARDS_DISTRIBUTOR",
                string(abi.encode(virtual_distributor))
            ));
        }

        vm.stopBroadcast();
    }
}
