// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {IBaseNFT} from "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {IdentityMetadataProxyV1} from "src/nft/IdentityMetadataProxyV1.sol";
import {IdentityControllerV1} from "src/controller/IdentityControllerV1.sol";

contract DeployIdentityMetadataScript is Script {
    IdentityMetadataProxyV1 internal identityMetadata;
    IdentityControllerV1 internal identityController;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address identity_controller_address = vm.envAddress("IDENTITY_CONTROLLER_ADDRESS");
        identityController = IdentityControllerV1(payable(identity_controller_address));
        identityMetadata = new IdentityMetadataProxyV1();
        console.log("IdentityMetadataProxyV1: ");
        console.log(address(identityMetadata));
        IBaseNFT nft = identityController.base();
        nft.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setProxyConfig(bytes32,address,bytes4,address)",
            nft.baseNode(),
            address(nft),
            bytes4(keccak256("tokenURI(uint256,string,string)")),
            address(identityMetadata)
        ));  
        vm.stopBroadcast();
    }
}
