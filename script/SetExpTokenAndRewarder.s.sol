// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {AvatarNFTV1} from "src/nft/AvatarNFTV1.sol";

contract SetExpTokenAndRewarderScript is Script {
    bytes32 internal avatarBasenode = 0x46f49b6a04b289d8ac4920f8a236e64e2689abdcf283566b399ff3bcfb6d967a;

    AvatarNFTV1 internal avatarNFT;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);

        address avatar_nft_address = vm.envAddress("AVATAR_NFT_ADDRESS");
        avatarNFT = AvatarNFTV1(avatar_nft_address);

        // Set leveling rules
        address exp_token = vm.envAddress("EXP_TOKEN_ADDRESS");
        if (exp_token != address(0)) {
            avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setProjectLevelingRules(bytes32,address,address,uint256,bytes4)",
                avatarBasenode,
                address(0),
                exp_token,
                0,
                bytes4(0xf5298aca)
            ));
        }

        // Set level metadata
        address virtual_distributor = vm.envAddress("VIRTUAL_DIST_ADDRESS");
        if (virtual_distributor != address(0)) {
            avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setText(bytes32,string,string)",
                avatarBasenode,
                "L7L_REWARDS_DISTRIBUTOR",
                string(abi.encode(virtual_distributor))
            ));
        }
        vm.stopBroadcast();
    }
}