// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {IBaseNFT} from "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {ContributorMetadataProxyV1} from "src/nft/ContributorMetadataProxyV1.sol";
import {ContributorControllerV1} from "src/controller/ContributorControllerV1.sol";
import {ContributorNFTV1} from "src/nft/ContributorNFTV1.sol";

contract DeployContributorMetadataScript is Script {
    ContributorMetadataProxyV1 internal contributorMetadata;
    ContributorControllerV1 internal contributorController;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address contributor_controller_address = vm.envAddress("CONTRIBUTOR_CONTROLLER_ADDRESS");
        contributorController = ContributorControllerV1(payable(contributor_controller_address));
        contributorMetadata = new ContributorMetadataProxyV1(contributorController.crs(), contributorController.baseNode());
        console.log("ContributorMetadataProxyV1: ");
        console.log(address(contributorMetadata));
        IBaseNFT nft = contributorController.base();
        nft.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setProxyConfig(bytes32,address,bytes4,address)",
            nft.baseNode(),
            address(nft),
            bytes4(keccak256("tokenURI(uint256,string,string)")),
            address(contributorMetadata)
        ));  
        vm.stopBroadcast();
    }
}
