// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {InvestorNFTMinterV1} from "src/minter/InvestorNFTMinterV1.sol";
import {InvestorNFTV1} from "src/nft/InvestorNFTV1.sol";

contract DeployInvestorScript is Script {
    InvestorNFTMinterV1 public investorMinter;
    InvestorNFTV1 public investorNFT;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address _ownerAddress = vm.envAddress("INVESTOR_OWNER_ADDRESS");
        address _beneficiaryAddress = vm.envAddress("INVESTOR_BENEFICIARY_ADDRESS");
        bytes32 _merkleRoot = vm.envBytes32("INITIAL_MERKLE_ROOT");

        investorMinter = new InvestorNFTMinterV1(_ownerAddress, payable(_beneficiaryAddress));
        console.log("InvestorNFTMinterV1: ");
        console.log(address(investorMinter));
        investorNFT = new InvestorNFTV1(address(investorMinter));
        console.log("investorNFTV1: ");
        console.log(address(investorNFT));
        investorMinter.ownerSetNFTContract(address(investorNFT), _merkleRoot);
        investorMinter.ownerSetPhase(3, _merkleRoot);

        vm.stopBroadcast();
    }
}
