// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import "@le7el/web3_crs/contracts/registry/CRSRegistry.sol";
import "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {AvatarControllerV1} from "src/controller/AvatarControllerV1.sol";
import {IdentityControllerV1} from "src/controller/IdentityControllerV1.sol";
import {AvatarNFTV1} from "src/nft/AvatarNFTV1.sol";
import {IdentityNFTV1} from "src/nft/IdentityNFTV1.sol";
import {LevelingResolver} from "src/resolver/LevelingResolver.sol";

contract DeployAllScript is Script {
    bytes32 internal l7lBasenode = 0xd0340a34011af087b374bbdc5136a48a841af1b55b0af1143ced23c89cf182c9;
    bytes32 internal l7lHash = 0x37372c33bd16695ac1a42f79a40d119a436fa3a44dc50a9117f2380fa5d80acc;
    bytes32 internal avatarBasenode = 0x46f49b6a04b289d8ac4920f8a236e64e2689abdcf283566b399ff3bcfb6d967a;
    bytes32 internal avatarHash = 0xd1f86c93d831119ad98fe983e643a7431e4ac992e3ead6e3007f4dd1adf66343;
    CRSRegistry internal crs;
    LevelingResolver internal resolver;
    IdentityNFTV1 internal identityNFT;
    AvatarNFTV1 internal avatarNFT;
    IdentityControllerV1 internal identityController;
    AvatarControllerV1 internal avatarController;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address crs_address = vm.envAddress("CRS_REGISTRY_ADDRESS");
        if (crs_address != address(0)) {
            crs = CRSRegistry(crs_address);
        } else {
            crs = new CRSRegistry();
            console.log("CRS registry: ");
            console.log(address(crs));
            crs.setSubnodeOwner(bytes32(0), l7lHash, msg.sender);
            crs.setSubnodeOwner(bytes32(0), avatarHash, msg.sender);
        }
        address resolver_address = vm.envAddress("CRS_RESOLVER_ADDRESS");
        if (resolver_address != address(0)) {
            resolver = LevelingResolver(resolver_address);
        } else {
            resolver = new LevelingResolver(crs);
            console.log("Resolver: ");
            console.log(address(resolver));
        }

        identityNFT = new IdentityNFTV1(crs, l7lBasenode);
        console.log("Identity NFT: ");
        console.log(address(identityNFT));
        avatarNFT = new AvatarNFTV1(crs, avatarBasenode);
        console.log("Avatar NFT: ");
        console.log(address(avatarNFT));
        identityController = new IdentityControllerV1(identityNFT);
        console.log("Identity NFT controller: ");
        console.log(address(identityController));
        avatarController = new AvatarControllerV1(avatarNFT);
        console.log("Avatar NFT controller: ");
        console.log(address(avatarController));
        identityNFT.addController(address(identityController));
        avatarNFT.addController(address(avatarController));

        // Will fail if you try to override one of existing zones
        crs.setResolver(l7lBasenode, address(resolver));
        crs.setResolver(avatarBasenode, address(resolver));
        resolver.setRoyalties(l7lBasenode, address(identityController), 0, address(0), address(0)); // free
        resolver.setRoyalties(avatarBasenode, address(avatarController), 317097919, address(0), address(0)); // 0.01 gas coin per year
        crs.setSubnodeOwner(bytes32(0), l7lHash, address(identityNFT));
        crs.setSubnodeOwner(bytes32(0), avatarHash, address(avatarNFT));

        // Set leveling rules
        address exp_token = vm.envAddress("EXP_TOKEN_ADDRESS");
        if (exp_token != address(0)) {
            avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setProjectLevelingRules(bytes32,address,address,uint256,bytes4)",
                avatarBasenode,
                address(0),
                exp_token,
                0,
                bytes4(0xf5298aca)
            ));
        }

        // Set level metadata
        address virtual_distributor = vm.envAddress("VIRTUAL_DIST_ADDRESS");
        if (virtual_distributor != address(0)) {
            avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
                "setText(bytes32,string,string)",
                avatarBasenode,
                "L7L_REWARDS_DISTRIBUTOR",
                string(abi.encode(virtual_distributor))
            ));
        }

        vm.stopBroadcast();
    }
}
