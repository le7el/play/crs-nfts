// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import "@le7el/web3_crs/contracts/registry/CRSRegistry.sol";
import {AvatarNFTV1} from "src/nft/AvatarNFTV1.sol";
import {IdentityNFTV1} from "src/nft/IdentityNFTV1.sol";
import {AvatarControllerV1} from "src/controller/AvatarControllerV1.sol";
import {LevelingResolver} from "src/resolver/LevelingResolver.sol";

contract ChangeResolverScript is Script {
    bytes32 internal l7lBasenode = 0xd0340a34011af087b374bbdc5136a48a841af1b55b0af1143ced23c89cf182c9;
    bytes32 internal avatarBasenode = 0x46f49b6a04b289d8ac4920f8a236e64e2689abdcf283566b399ff3bcfb6d967a;

    CRSRegistry internal crs;
    LevelingResolver internal resolver;
    AvatarControllerV1 internal avatarController;
    AvatarNFTV1 internal avatarNFT;
    IdentityNFTV1 internal identityNFT;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address exp_token = vm.envAddress("EXP_TOKEN_ADDRESS");
        address avatar_nft_address = vm.envAddress("AVATAR_NFT_ADDRESS");
        address identity_nft_address = vm.envAddress("IDENTITY_NFT_ADDRESS");
        avatarNFT = AvatarNFTV1(avatar_nft_address);
        identityNFT = IdentityNFTV1(identity_nft_address);

        address crs_address = vm.envAddress("CRS_REGISTRY_ADDRESS");
        crs = CRSRegistry(crs_address);

        address identity_controller_address = vm.envAddress("IDENTITY_CONTROLLER_ADDRESS");
        address avatar_controller_address = vm.envAddress("AVATAR_CONTROLLER_ADDRESS");
        avatarController = AvatarControllerV1(payable(avatar_controller_address));

        resolver = new LevelingResolver(crs);
        console.log("Resolver: ");
        console.log(address(resolver));
        avatarNFT.setResolver(address(resolver));
        avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setRoyalties(address,uint256,address,address)",
            avatar_controller_address,
            3168873850,  // 1 gas coin per year
            address(0),
            address(0)
        ));
        avatarNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setProjectLevelingRules(bytes32,address,address,uint256,bytes4)",
            avatarBasenode,
            address(0),
            exp_token,
            0,
            bytes4(0xf5298aca)
        ));  
        identityNFT.setResolver(address(resolver));
        identityNFT.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setRoyalties(address,uint256,address,address)",
            identity_controller_address,
            0,  // free
            address(0),
            address(0)
        ));
        vm.stopBroadcast();
    }
}