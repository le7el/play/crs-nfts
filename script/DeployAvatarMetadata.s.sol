// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {IBaseNFT} from "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {AvatarMetadataProxyV1} from "src/nft/AvatarMetadataProxyV1.sol";
import {AvatarControllerV1} from "src/controller/AvatarControllerV1.sol";
import {AvatarNFTV1} from "src/nft/AvatarNFTV1.sol";

contract DeployAvatarMetadataScript is Script {
    AvatarMetadataProxyV1 internal avatarMetadata;
    AvatarControllerV1 internal avatarController;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address avatar_controller_address = vm.envAddress("AVATAR_CONTROLLER_ADDRESS");
        avatarController = AvatarControllerV1(payable(avatar_controller_address));
        avatarMetadata = new AvatarMetadataProxyV1(avatarController.crs(), avatarController.baseNode());
        console.log("AvatarMetadataProxyV1: ");
        console.log(address(avatarMetadata));
        IBaseNFT nft = avatarController.base();
        nft.setBasenodeResolverSettings(abi.encodeWithSignature(
            "setProxyConfig(bytes32,address,bytes4,address)",
            nft.baseNode(),
            address(nft),
            bytes4(keccak256("tokenURI(uint256,string,string)")),
            address(avatarMetadata)
        ));  
        vm.stopBroadcast();
    }
}
