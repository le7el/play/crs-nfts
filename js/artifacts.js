import registry from "../out/CRSRegistry.sol/CRSRegistry.json"
import resolver from "../out/LevelResolver.sol/LevelResolver.json"
import identityControllerV1 from "../out/IdentityControllerV1.sol/IdentityControllerV1.json"
import avatarControllerV1 from "../out/AvatarControllerV1.sol/AvatarControllerV1.json"
import identityNFTV1 from "../out/IdentityNFTV1.sol/IdentityNFTV1.json"
import avatarNFTV1 from "../out/AvatarNFTV1.sol/AvatarNFTV1.json"
import investorNFTV1 from "../out/InvestorNFTV1.sol/InvestorNFTV1.json"
import investorNFTMinterV1 from "../out/InvestorNFTMinterV1.sol/InvestorNFTMinterV1.json"

export default {
  registry,
  resolver,
  identityControllerV1,
  avatarControllerV1,
  identityNFTV1,
  avatarNFTV1,
  investorNFTV1,
  investorNFTMinterV1
}