import artifacts from './artifacts'
import contracts from './contracts'
import { ethers, controller, resolver, nft, utils } from '@le7el/web3_crs'
import { assertExpTokensContractDeployed } from '@le7el/experience_tokens'

const CURRENT_VERSION = 'v1'
const CURRENT_INVESTOR_NFT_VERSION = 'v1'
const LE7EL_PROJECT_NODE = '0x46f49b6a04b289d8ac4920f8a236e64e2689abdcf283566b399ff3bcfb6d967a' // .avatar
const ERC1155_APPROVE_ABI = [
  "function setApprovalForAll(address operator, bool approved) public",
  "function isApprovedForAll(address, address) public view returns (bool)"
]

const assertContractDeployed = (chainId, contractName, version = CURRENT_VERSION) => {
  if (contracts[`${chainId}`] && contracts[`${chainId}`][version] && contracts[`${chainId}`][version][contractName]) {
    return Promise.resolve(contracts[chainId][version][contractName])
  }
  return Promise.reject(`contract ${contractName}:${version} is not deployed to network ${chainId}`);
}

const _wrapControllerCall = (chainId, provider, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      params.push(provider)
      return controller[fnName].apply(this, params)
    })

const _wrapResolverCall = (chainId, provider, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      params.push(provider)
      return resolver[fnName].apply(this, params)
    })

const _wrapNftCall = (chainId, provider, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      params.push(provider)
      return nft[fnName].apply(this, params)
    })

const _initResolverContract = (chainId, provider = null, readOnly = true) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((contract_address) => utils.initContract(contract_address, artifacts.resolver.abi, readOnly, provider))

const _maybeErc1155Approve = (token_address, wallet_address, spender_address, provider = null) =>
  utils.initContract(token_address, ERC1155_APPROVE_ABI, false, provider)
    .then((contract) =>
      contract.isApprovedForAll(wallet_address, spender_address)
        .then((approved) => {
          if (approved) return Promise.resolve(true)
          return contract.setApprovalForAll(spender_address, true)
        })
    )

// CRS NFTs
const identityAvailable = (name, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'identity_controller', 'available', [name])

const avatarAvailable = (name, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'available', [name])

const contributorAvailable = (name, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'contributor_controller', 'available', [name])

const identityRentPrice = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'identity_controller', 'rentPrice', [name, duration])

const avatarRentPrice = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'rentPrice', [name, duration])

const contributorRentPrice = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'contributor_controller', 'rentPrice', [name, duration])

const identityDomainSeparator = (chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'identity_controller', 'getDomainSeparator', [])

const avatarDomainSeparator = (chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'getDomainSeparator', [])

const contributorDomainSeparator = (chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'contributor_controller', 'getDomainSeparator', [])

const makeIdentityCommitment = (name, owner, secret, chainId, provider = null) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => _wrapControllerCall(chainId, provider, 'identity_controller', 'makeCommitment', [name, owner, secret, resolver, owner]))

const makeAvatarCommitment = (name, owner, secret, chainId, provider = null) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => _wrapControllerCall(chainId, provider, 'avatar_controller', 'makeCommitment', [name, owner, secret, resolver, owner]))

const makeContributorCommitment = (name, owner, secret, chainId, provider = null) =>
    assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
      .then((resolver) => _wrapControllerCall(chainId, provider, 'contributor_controller', 'makeCommitment', [name, owner, secret, resolver, owner]))

const commitIdentity = (name, commitment, pass, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'identity_controller', 'whitelistedCommit', [name, commitment, pass])

const commitAvatar = (commitment, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'commit', [commitment])

const commitAvatarWhitelisted = (name, commitment, pass, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'whitelistedCommit', [name, commitment, pass])

const commitContributor = (name, commitment, pass, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'contributor_controller', 'whitelistedCommit', [name, commitment, pass])

const registerIdentity = (name, owner, duration, secret, chainId, provider = null) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => _wrapControllerCall(chainId, provider, 'identity_controller', 'register', [name, owner, duration, secret, resolver, owner]))

const registerAvatar = (name, owner, duration, secret, chainId, provider = null) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => _wrapControllerCall(chainId, provider, 'avatar_controller', 'register', [name, owner, duration, secret, resolver, owner]))

const registerContributor = (name, owner, duration, secret, chainId, provider = null) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => _wrapControllerCall(chainId, provider, 'contributor_controller', 'register', [name, owner, duration, secret, resolver, owner]))

const renewAvatar = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'avatar_controller', 'renew', [name, duration])

const renewIdentity = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'identity_controller', 'renew', [name, duration])

const renewContributor = (name, duration, chainId, provider = null) =>
  _wrapControllerCall(chainId, provider, 'contributor_controller', 'renew', [name, duration])

const setText = (node, key, value, chainId, provider = null) =>
  _wrapResolverCall(chainId, provider, 'resolver', 'setText', [node, key, value])

const getText = (node, key, chainId, provider = null) =>
  _wrapResolverCall(chainId, provider, 'resolver', 'getText', [node, key])

const setNftImage = (node, newImageURL, chainId, provider = null) =>
  setText(node, 'L7L_AVATAR_IMAGE', newImageURL, chainId, provider)

const getNftImage = (node, chainId, provider = null) =>
  getText(node, 'L7L_AVATAR_IMAGE', chainId, provider)

const getAvatarGasFee = (duration, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'avatar_nft', 'gasFee', [duration])

const getIdentityGasFee = (duration, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'identity_nft', 'gasFee', [duration])

const getContributorGasFee = (duration, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'contributor_nft', 'gasFee', [duration])

const getAvatarNameExpires = (name, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'avatar_nft', 'getNameExpires', [name])

const getIdentityNameExpires = (name, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'identity_nft', 'getNameExpires', [name])

const getContributorNameExpires = (name, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'contributor_nft', 'getNameExpires', [name])

const getAvatarNameByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'avatar_nft', 'getName', [id])

const getIdentityNameByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'identity_nft', 'getName', [id])

const getContributorNameByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'contributor_nft', 'getName', [id])

const getAvatarMetadataByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'avatar_nft', 'getMetadata', [id])

const getIdentityMetadataByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'identity_nft', 'getMetadata', [id])

const getContributorMetadataByNftId = (id, chainId, provider = null) =>
  _wrapNftCall(chainId, provider, 'contributor_nft', 'getMetadata', [id])

const getAvatarNftsForWallet = (wallet, from_block, to_block, queries_per_batch, chainId, provider = null) =>
  assertContractDeployed(chainId, 'avatar_nft', CURRENT_VERSION)
    .then(contractAddress => nft.getNftsForWallet(wallet, contractAddress, from_block, to_block, queries_per_batch, provider))

const getIdentityNftsForWallet = (wallet, from_block, to_block, queries_per_batch, chainId, provider = null) =>
  assertContractDeployed(chainId, 'identity_nft', CURRENT_VERSION)
    .then(contractAddress => nft.getNftsForWallet(wallet, contractAddress, from_block, to_block, queries_per_batch, provider))

const getContributorNftsForWallet = (wallet, from_block, to_block, queries_per_batch, chainId, provider = null) =>
    assertContractDeployed(chainId, 'contributor_nft', CURRENT_VERSION)
      .then(contractAddress => nft.getNftsForWallet(wallet, contractAddress, from_block, to_block, queries_per_batch, provider))

const getProjectExperience = (projectNode, node, chainId, provider = null) =>
  _initResolverContract(chainId, provider)
    .then((contract) => contract.experience(projectNode, node))

const getProjectLevel = (projectNode, node, chainId, provider = null) =>
  _initResolverContract(chainId, provider)
    .then((contract) => contract.level(projectNode, node))

const advanceToNextLevel = (projectNode, node, experienceToBurn, chainId, provider = null) =>
  _initResolverContract(chainId, provider, false)
    .then((contract) => contract.advanceToNextLevel(projectNode, node, experienceToBurn))

const getLe7elExperience = (node, chainId, provider = null) =>
  getProjectExperience(LE7EL_PROJECT_NODE, node, chainId, provider)

const getLe7elLevel = (node, chainId, provider = null) =>
  getProjectLevel(LE7EL_PROJECT_NODE, node, chainId, provider)

const advanceToNextLe7elLevel = (node, experienceToBurn, chainId, provider = null) =>
  advanceToNextLevel(LE7EL_PROJECT_NODE, node, experienceToBurn, chainId, provider)

// Investor NFT helpers
const _wrapInvestorNftMinterCall = (chainId, provider, contract, fnName, params, overrides = null) =>
  assertContractDeployed(chainId, contract, CURRENT_INVESTOR_NFT_VERSION)
    .then((contractAddress) => {
      contract = new ethers.Contract(contractAddress, artifacts.investorNFTMinterV1.abi, provider)
      if (overrides) params.push(overrides)
      return contract[fnName].apply(this, params)
    })

const mintInvestorNft = (owner, merkleIndex, merkleProof, chainId, provider) =>
  _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'price', [])
    .then((price) => _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'mintTo(address,uint256,bytes32[])', [owner, merkleIndex, merkleProof], {value: price}))

const mintPublicInvestorNft = (owner, chainId, provider) => 
  _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'pricePublic', [])
    .then((price) => _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'mintTo(address)', [owner], {value: price}))
  
const isClaimedInvestorNftProof = (index, chainId, provider) =>
  _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'isClaimed', [index])

const investorNftMintPhase = (chainId, provider) =>
  _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'phase', [])

const investorSetNftMintPhase = (phase, merkleRoot, chainId, provider) =>
  _wrapInvestorNftMinterCall(chainId, provider, 'investor_nft_minter', 'ownerSetPhase', [phase, merkleRoot])

export {
  utils,
  artifacts,
  contracts,
  assertContractDeployed,
  identityAvailable,
  avatarAvailable,
  contributorAvailable,
  identityRentPrice,
  avatarRentPrice,
  contributorRentPrice,
  identityDomainSeparator,
  avatarDomainSeparator,
  contributorDomainSeparator,
  makeIdentityCommitment,
  makeAvatarCommitment,
  makeContributorCommitment,
  commitIdentity,
  commitAvatar,
  commitAvatarWhitelisted,
  commitContributor,
  registerIdentity,
  registerAvatar,
  registerContributor,
  renewAvatar,
  renewIdentity,
  renewContributor,
  setText,
  getText,
  setNftImage,
  getNftImage,
  getAvatarNftsForWallet,
  getIdentityNftsForWallet,
  getContributorNftsForWallet,
  getAvatarGasFee,
  getIdentityGasFee,
  getContributorGasFee,
  getAvatarNameExpires,
  getIdentityNameExpires,
  getContributorNameExpires,
  getAvatarMetadataByNftId,
  getIdentityMetadataByNftId,
  getContributorMetadataByNftId,
  getAvatarNameByNftId,
  getIdentityNameByNftId,
  getContributorNameByNftId,
  getProjectExperience,
  getProjectLevel,
  advanceToNextLevel,
  getLe7elExperience,
  getLe7elLevel,
  advanceToNextLe7elLevel,
  mintInvestorNft,
  mintPublicInvestorNft,
  isClaimedInvestorNftProof,
  investorNftMintPhase,
  investorSetNftMintPhase
}