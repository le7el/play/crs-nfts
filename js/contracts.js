export default {
  '1': {
    'v1': {
      registry: "0xc566b9Ce143B73704Ca75AE3e9bDBDC5d069B3cF",
      resolver: "0x7713DD819D6710b8B6D346Bd3470Bb52655824A5",
      contributor_controller: "0x1B60e4A71f91590Db733aDe6A1b72F8D50d5fc73",
      contributor_nft: "0x351Cd141D53c2A7ca8FCAdc90af2470469a8d1B1",
      investor_nft: "0xf4E28D3F6167D416b1FDC248C82AA59263f52a6d",
      investor_nft_minter: "0x4f047dE021cB38f0C6C6b45567658073AC91C605"
    }
  },
  '4': {
    'v1': {
      registry: "0xf754b8De91Bd619227B5DF74c760B58048A4095D",
      resolver: "0x29f24ba9813445b48013d13dc7e20b560eadd825",
      identity_controller: "0x9434b086fdd4cf118d2288a7257b3dd52b6c754a",
      identity_nft: "0xf02153c5d574bb1bc7f4032280c4989ec451442d",
      avatar_controller: "0x1dd23e65617c2ea9cb8b0a8e56e9df9d3f8adbf6",
      avatar_nft: "0x4da49b2e09f7c1ba40626343d4cb97da78414ebc"
    }
  },
  '5': {
    'v1': {
      registry: "0x4246D62c958D5a8d5C95A6841E70562Bb13F627f",
      resolver: "0x433162aA72a980117aa38A21EC383EFB91cd9374",
      identity_controller: "0x7d9F75f2BEC682a134610F92d54ba580A142D9f6",
      identity_nft: "0x3e72fBd0cf67E02e022c71c555A59d4D891832D4",
      avatar_controller: "0xb2F44a14f7DbBE0EDbe3D8889a9b538EcF4069Db",
      avatar_nft: "0x8867AaeE03b9e573a300CCa93Af1F79c53262eBf",
      investor_nft: "0xEF15D863F87B95127b1b1E66eAF6C13504643b3E",
      investor_nft_minter: "0x8E897DB70C3d7c5daEB0e93BEDbc86CD2AfF38A3"
    }
  },
  '11155111': {
    'v1': {
      registry: "0x702b0d7B1ae097fB59f4CD8979aec7C06F68c626",
      resolver: "0x86c5646440bA6D0BfaAF08853BCA4F58528f3327",
      identity_controller: "0x4B08725B4EdD7a41924888204798FFA14e85c41c",
      identity_nft: "0x3C34AF6DDafB21C3A9293F1356E3684bF3d5a1bD",
      avatar_controller: "0x5682faA9D58BFfa0d348dc144995bF3DaF9666de",
      avatar_nft: "0xc7F81E795FE8eA3242dF960428E54398C32Aadfc",
      contributor_controller: "0x8A9eB20C3d193B0909966B7b65fE57a7a2d27496",
      contributor_nft: "0x68Fe087B98a42620A021AA51BAFAf3cB5952680F",
      investor_nft: "0xd8f803bFB0Fb6f4717A2791999884d39f92d2095",
      investor_nft_minter: "0x54350483A9CBBCb9C3C1795Ff4c42D26E43442dF"
    }
  },
  '137': {
    'v1': {
      registry: "0xDF4D2C1d3F9B960501deFDBDB9f7D32361E4C72F",
      resolver: "0xc53D1853A87860B8CF9D14dC302A75E6198697B8",
      identity_controller: "0xE5Fe2D10697588E34c7aA6902cB51ae7f7562409",
      identity_nft: "0x89915792efC73A098fDfF8F9Cc152524Efcda5AB",
      avatar_controller: "0x88C39c7dB366D40154f00644De8c19F98f7C633D",
      avatar_nft: "0xc477144C19bcaec754109B5b96f2046302850cbC"
    }
  }
}