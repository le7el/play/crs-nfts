import { BalanceTree } from "@le7el/rewards_engine"
import { ethers } from '@le7el/web3_crs'
const BigNumber = ethers.BigNumber

import {
  utils,
  identityRentPrice,
  avatarRentPrice,
  contributorRentPrice,
  identityDomainSeparator,
  contributorDomainSeparator,
  makeIdentityCommitment,
  makeAvatarCommitment,
  makeContributorCommitment,
  commitIdentity,
  commitAvatar,
  commitContributor,
  registerIdentity,
  registerAvatar,
  registerContributor,
  setText,
  getText,
  getLe7elExperience,
  getLe7elLevel,
  advanceToNextLe7elLevel,
  assertContractDeployed,
  mintInvestorNft,
  mintPublicInvestorNft,
  isClaimedInvestorNftProof,
  investorNftMintPhase,
  investorSetNftMintPhase
} from './lib'

const { erc20Approve, nameToNode } = utils

const CHAIN_ID = '11155111'
const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000'
const PRIV_KEY = '888fa71d782f31e9d1c952ab74d23a0f8f3f4dc189b8165a94810cf62c805af8' // '0x11169009E2E4956205632177ba1d2F2603342D91'

const whitelist = async function(address, name) {
  const whitelistMessage = ethers.utils.keccak256(
    ethers.utils.defaultAbiCoder.encode(
      ["bytes32", "address", "string"], [
      await identityDomainSeparator(CHAIN_ID),
      address,
      name
    ],
    ),
  )

  const signer = new ethers.Wallet(PRIV_KEY)
  const whitelistSignature = await signer.signMessage(ethers.utils.arrayify(whitelistMessage))
  const sig = ethers.utils.splitSignature(
    ethers.utils.arrayify(whitelistSignature)
  );
  const whitelistSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
    ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
  )
  return whitelistSignatureEncoded
}

const whitelistContributor = async function(address, name) {
  const whitelistMessage = ethers.utils.keccak256(
    ethers.utils.defaultAbiCoder.encode(
      ["bytes32", "address", "string"], [
      await contributorDomainSeparator(CHAIN_ID),
      address,
      name
    ],
    ),
  )

  const signer = new ethers.Wallet(PRIV_KEY)
  const whitelistSignature = await signer.signMessage(ethers.utils.arrayify(whitelistMessage))
  const sig = ethers.utils.splitSignature(
    ethers.utils.arrayify(whitelistSignature)
  );
  const whitelistSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
    ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
  )
  return whitelistSignatureEncoded
}

document.getElementById('crs_commit').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  let registrant;
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const secret = ethers.utils.randomBytes(32)
  document.getElementById('crs_secret').value = ethers.utils.hexlify(secret)

  provider.listAccounts()
    .then((accounts) => {
      if (accounts.length === 0) {
        return provider.send("eth_requestAccounts")
      }

      return accounts
    })
    .then((accounts) => {
      registrant = accounts[0];
      if (controller === 'identity_controller') {
        return makeIdentityCommitment(name, registrant, secret, CHAIN_ID)
      } else if (controller === 'contributor_controller') {
        return makeContributorCommitment(name, registrant, secret, CHAIN_ID)
      } else {
        return makeAvatarCommitment(name, registrant, secret, CHAIN_ID)
      }
    })
    .then((commitment) => {
      document.getElementById('crs_reg_name').value = name;
      document.getElementById('crs_commitment').value = commitment;
      if (controller === 'identity_controller') {
        return whitelist(registrant, name)
          .then((pass) => {
            return commitIdentity(name, commitment, pass, CHAIN_ID)
          })
      } else if (controller === 'contributor_controller') {
        return whitelistContributor(registrant, name)
          .then((pass) => {
            return commitContributor(name, commitment, pass, CHAIN_ID)
          })
      } else {
        return commitAvatar(commitment, CHAIN_ID)
      }
    })
    .then((tr) => {
      document.getElementById('crs_commit_mining').style.display = 'block';
      return provider.waitForTransaction(tr.hash)
    })
    .then(() => {
      document.getElementById('crs_commit_mining').style.display = 'none';
      const timerNode = document.getElementById('crs_maturation');
      timerNode.parentNode.style.display = 'block';
      let timer;
      timer = setInterval(() => {
        timerNode.innerHTML = parseInt(timerNode.innerHTML) - 1;
        if (timerNode.innerHTML === '0') {
          document.getElementById('crs_commit').disabled = true;
          document.getElementById('crs_register').disabled = false;
          timerNode.parentNode.style.display = 'none';
          clearInterval(timer);
        }
      }, 1000);
    })
})

document.getElementById('crs_register').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  const commitment = document.getElementById('crs_commitment').value
  if (!commitment) return

  document.getElementById('crs_done').innerHTML = 'Please, refresh the page to register a new NFT';
  document.getElementById('crs_done').style.display = 'none';

  const secret = document.getElementById('crs_secret').value
  const duration = document.getElementById('crs_duration').value

  const provider = new ethers.providers.Web3Provider(window.ethereum)
  provider.listAccounts()
    .then((accounts) => {
      let rentPromise;
      if (controller === 'identity_controller') {
        rentPromise = identityRentPrice(name, duration, CHAIN_ID)
      } else if (controller === 'contributor_controller') {
        rentPromise = contributorRentPrice(name, duration, CHAIN_ID)
      } else {
        rentPromise = avatarRentPrice(name, duration, CHAIN_ID)
      }

      return rentPromise
        .then((response) => {
          const token = response[0]
          const amount = response[1]
          if (token === ADDRESS_ZERO || response[1].isZero()) {
            if (controller === 'identity_controller') {
              return registerIdentity(name, accounts[0], duration, secret, CHAIN_ID)
            } else if (controller === 'contributor_controller') {
              return registerContributor(name, accounts[0], duration, secret, CHAIN_ID)
            } else {
              return registerAvatar(name, accounts[0], duration, secret, CHAIN_ID)
            }
          } else {
            return assertContractDeployed(CHAIN_ID, controller, 'v1')
              .then((spender) => {
                return erc20Approve(token, spender, amount)
                  .then(tr => provider.waitForTransaction(tr.hash))
                  .then(_ => {
                    if (controller === 'identity_controller') {
                      return registerIdentity(name, accounts[0], duration, secret, CHAIN_ID)
                    } else if (controller === 'contributor_controller') {
                      return registerContributor(name, accounts[0], duration, secret, CHAIN_ID)
                    } else {
                      return registerAvatar(name, accounts[0], duration, secret, CHAIN_ID)
                    }
                  })
              })
          }
        })
    })
    .then(() => {
      document.getElementById('crs_done').style.display = 'block';
    })
    .catch((error) => {
      document.getElementById('crs_done').innerHTML = error;
      document.getElementById('crs_done').style.display = 'block';
    })
})

function debounce(func, timeout = 300) {
  let timer
  return (...args) => {
    clearTimeout(timer)
    timer = setTimeout(() => { func.apply(this, args) }, timeout)
  }
}

const checkRecord = (event) => {
  const value = event.target.value
  if (!value) {
    document.getElementById('crs_manage_get').disabled = "disabled"
    document.getElementById('crs_manage_set').disabled = "disabled"
    document.getElementById('crs_manage_level_load').disabled = "disabled"
    document.getElementById('crs_manage_level_burn').disabled = "disabled"
    return
  }

  document.getElementById('crs_manage_get').disabled = false
  document.getElementById('crs_manage_set').disabled = false
  document.getElementById('crs_manage_level_load').disabled = false
  document.getElementById('crs_manage_level_burn').disabled = false
}
document.getElementById('crs_manage_name').addEventListener('change', debounce(checkRecord))

document.getElementById('crs_manage_set').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const value = document.getElementById('crs_manage_text_value').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !value || !name) return

  setText(node, key, value, CHAIN_ID)
})

document.getElementById('crs_manage_get').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !name) return

  getText(node, key, CHAIN_ID)
    .then((value) => document.getElementById('crs_manage_text_value').value = value)
})

document.getElementById('crs_manage_level_load').addEventListener('click', () => {
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!name) return

  getLe7elExperience(node, CHAIN_ID)
    .then((value) => document.getElementById('crs_manage_level_exp').value = value)

  getLe7elLevel(node, CHAIN_ID)
    .then((value) => document.getElementById('crs_manage_level_level').value = value)
})

document.getElementById('crs_manage_level_burn').addEventListener('click', () => {
  const exp = document.getElementById('crs_manage_level_burn_exp').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!exp || !name) return

  utils.getAccounts()
    .then((accounts) => {
      return advanceToNextLe7elLevel(accounts[0], node, exp, CHAIN_ID)
        .then(() => {
          return Promise.all([
            getLe7elExperience(node, CHAIN_ID)
              .then((value) => document.getElementById('crs_manage_level_exp').value = value),
            getLe7elLevel(node, CHAIN_ID)
              .then((value) => document.getElementById('crs_manage_level_level').value = value)
          ])
        })
    })
})

// Investor NFT testing
const provider = new ethers.providers.Web3Provider(window.ethereum)

const normaliseWallets = (wallets) =>
  wallets.split("\n").map((wallet) => wallet.trim().toLowerCase())

const buildMerkleTreeForWallets = (wallets) => {
  return new BalanceTree(normaliseWallets(wallets).map((wallet) => {
    return {"account": wallet, "amount": BigNumber.from(1)}
  }))
}

document.getElementById('inft_whitelist_apply').addEventListener('click', () => {
  const phase = document.getElementById('inft_phase').value
  provider.listAccounts()
    .then((accounts) => {
      if (accounts.length === 0) {
        return provider.send("eth_requestAccounts")
      }

      return accounts
    })
    .then((accounts) => {
      const wallets = document.getElementById('inft_whitelist').value
      const tree = buildMerkleTreeForWallets(wallets)
      const root = tree.getHexRoot()
      console.log("Merkle Root", root)
      return investorSetNftMintPhase(phase, root, 5, provider.getSigner())
    })
})

document.getElementById('inft_whitelist_proof').addEventListener('click', () => {
  const wallets = document.getElementById('inft_whitelist').value
  const tree = buildMerkleTreeForWallets(wallets)
  const parsed_wallets = normaliseWallets(wallets)
  const proofs = parsed_wallets.reduce((acc, wallet, index) => {
    const proof = tree.getProof(index, wallet, BigNumber.from(1))
    acc += "\n" + JSON.stringify(proof)
    return acc
  }, "")
  const root = tree.getHexRoot()
  console.log("Merkle Root", root)
  document.getElementById('inft_whitelist_proof_values').value = proofs.trim()
})

document.getElementById('inft_mint').addEventListener('click', () => {
  provider.listAccounts()
    .then((accounts) => {
      const wallets = document.getElementById('inft_whitelist').value
      const tree = buildMerkleTreeForWallets(wallets)
      const myWallet = accounts[0]
      const proof = tree.getProof(0, myWallet, BigNumber.from(1))
      return mintInvestorNft(accounts[0], 0, proof, 5, provider.getSigner())
    })
})
