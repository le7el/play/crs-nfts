// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import {ERC20BurnableMock} from "@openzeppelin/contracts/mocks/ERC20BurnableMock.sol";
import {ERC1155BurnableMock} from "@openzeppelin/contracts/mocks/ERC1155BurnableMock.sol";
import {MockERC20} from "solmate/test/utils/mocks/MockERC20.sol";
import {TestWithDeployments} from "./util/TestWithDeployments.sol";

contract IdentityNFTTest is TestWithDeployments {
    uint256 internal constant YEAR = 365 days;

    address internal admin = address(1);
    address internal user = address(2);

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user, "User");

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin);
        vm.stopPrank();

        // Register avatar NFT
        vm.startPrank(user);
        registerIdentityNFT(user, keccak256("secret"), YEAR, "johndoe");
        vm.stopPrank();
    }

    function testNoApprovalsByDefault() public {
        vm.startPrank(user);
        vm.expectRevert("Pausable: paused");
        identityNFT.approve(admin, 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        vm.expectRevert("Pausable: paused");
        identityNFT.setApprovalForAll(admin, true);
        vm.stopPrank();
    }

    function testTranfersWorkNormally() public {
        vm.startPrank(user);
        identityNFT.transferFrom(user, admin, 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        assertTrue(identityNFT.ownerOf(86745341786912841616557368118532256523691956314975099975829920571153145112669) == admin);
        vm.stopPrank();
    }

    function testSafeTranfersWorkNormally() public {
        vm.startPrank(user);
        identityNFT.safeTransferFrom(user, admin, 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        assertTrue(identityNFT.ownerOf(86745341786912841616557368118532256523691956314975099975829920571153145112669) == admin);
        vm.stopPrank();
    }

    function testUserCantEnableApprovals() public {
        vm.startPrank(user);
        vm.expectRevert("Ownable: caller is not the owner");
        identityNFT.ownerToggleApprovals(true);
        vm.stopPrank();
    }

    function testAdminCanToggleApprovals() public {
        vm.prank(admin);
        identityNFT.ownerToggleApprovals(true);
        vm.prank(user);
        identityNFT.approve(admin, 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        assertTrue(identityNFT.getApproved(86745341786912841616557368118532256523691956314975099975829920571153145112669) == admin);
        vm.prank(user);
        identityNFT.approve(address(0), 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        
        vm.prank(admin);
        identityNFT.ownerToggleApprovals(false);
        vm.startPrank(user);
        vm.expectRevert("Pausable: paused");
        identityNFT.approve(admin, 86745341786912841616557368118532256523691956314975099975829920571153145112669);
        vm.expectRevert("Pausable: paused");
        identityNFT.setApprovalForAll(admin, true);
        vm.stopPrank();
    }
}
